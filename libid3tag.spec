Name:           libid3tag
Version:        0.16.3
Release:        3
Summary:        ID3 tag manipulation library
License:        GPL-2.0-or-later
URL:            https://codeberg.org/tenacityteam/libid3tag
Source0:        https://codeberg.org/tenacityteam/libid3tag/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz
Patch0:         Add_unversioned_so.patch
Patch1:         libid3tag-0.16.3-port-to-newer-cmake.patch
BuildRequires:  gcc-c++ cmake make zlib-devel >= 1.1.4 gperf >= 3.1

%description
libid3tag is a library for reading and (eventually) writing ID3 tags,
both ID3v1 and the various versions of ID3v2.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
ID3 tag library development files.

%prep
%autosetup -p1 -n %{name}

%build
%cmake
%cmake_build

%install
%cmake_install

%files
%license COPYING COPYRIGHT
%doc CHANGES CREDITS README TODO
%{_libdir}/libid3tag.so.*

%files devel
%{_includedir}/id3tag.h
%{_libdir}/libid3tag.so
%{_libdir}/cmake/id3tag
%{_libdir}/pkgconfig/id3tag.pc


%changelog
* Thu Mar 06 2025 Funda Wang <fundawang@yeah.net> - 0.16.3-3
- build with cmake 4.0

* Tue Nov 19 2024 Funda Wang <fundawang@yeah.net> - 0.16.3-2
- adopt to new cmake macro

* Wed Oct 18 2023 chenyaqiang <chengyaqiang@huawei.com> - 0.16.3-1
- update to 0.16.3

* Wed Jul 15 2020 zhangjiapeng<zhangjiapeng9@huawei.com> - 0.15.1b-21
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: fix the build issue with gperf-3.1

* Fri Mar 20 2020 lingsheng<lingsheng@huawei.com> - 0.15.1b-20
- Type:cves
- ID:CVE-2017-11550
- SUG:restart
- DESC: fix CVE-2017-11550

* Tue Dec 24 2019 daiqianwen<daiqianwen@huawei.com> - 0.15.1b-19
- Type:cves
- ID:CVE-2004-2779
- SUG:restart
- DESC: fix CVE-2004-2779

* Sat Dec 7 2018 openEuler Buildteam <buildteam@openeuler.org> - 0.15.1b-18
- Package init
